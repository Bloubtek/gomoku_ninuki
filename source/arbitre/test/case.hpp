//
// case.cpp for case for gomoku in /home/hauteb_a/rendu/gomoku/source/arbitre
// 
// Made by Arthur Hautebas
// Login   <hauteb_a@epitech.net>
// 
// Started on  Wed Nov 18 16:06:03 2015 Arthur Hautebas
// Last update Wed Nov 18 17:42:02 2015 Arthur Hautebas
//

#ifndef CASE_HPP_
# define CASE_HPP_


# include <bitset>

class Case
{
  std::bitset<49>	blanc;
  std::bitset<49>	noir;
  std::bitset<49>	mur;
  int			status;
public:
  Case();
  ~Case();
  int	set_status();
  void	get_status(int);
  std::bitset<49>	get_map_blanc() const;
  std::bitset<49>	get_map_noir() const;
  std::bitset<49>	get_map_mur() const;
  void			set_map_blanc(std::bitset<49>);
  void			set_map_noir(std::bitset<49>);
  void			set_map_mur(std::bitset<49>);
};

#endif /* !CASE_HPP_ */
