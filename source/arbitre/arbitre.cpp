//
// arbitre.cpp for gomoku in /home/hauteb_a/rendu/gomoku/source/arbitre
// 
// Made by Arthur Hautebas
// Login   <hauteb_a@epitech.net>
// 
// Started on  Mon Nov  9 14:21:32 2015 Arthur Hautebas
// Last update Wed Nov 25 13:17:02 2015 Arthur Hautebas
//

#include "arbitre.hpp"

Arbitre::Arbitre()
{
  this->masks.push_back(std::bitset<49>(std::string(V_M_BT_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_E_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_E_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_SE_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_SE_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_S_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_S_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_SW_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_SW_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_W_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_W_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_NW_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_NW_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_N_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_N_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_NE_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_PP_NE_B)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_E_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_SE_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_S_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_SW_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_W_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_NW_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_N_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C0_NE_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_E_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_SE_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_S_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_SW_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_W_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_NW_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_N_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C1_NE_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C2_H_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C2_HV_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C2_V_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_C2_VH_A)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_E)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_SE)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_S)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_SW)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_W)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_NW)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_N)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P1_NE)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_E)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_SE)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_S)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_SW)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_W)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_NW)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_N)));
  this->masks.push_back(std::bitset<49>(std::string(V_M_P2_NE)));
}

Arbitre::~Arbitre()
{
}

int     Arbitre::to_tab(int x, int y, int i)
{
  return ((x + (i / 7) - 3) * 19 + y + (i % 7 - 3)); // 0 0 28 16 
}

void	Arbitre::add_stone(std::unordered_map<int, struct s_t> &map,
			   int x, int y, int player)
{
  std::bitset<49>	mask(std::string(M_MOD_MAP));
  int			i = 0;
  int			coord;

  if (player == PLAYER_BLANC)
    while (i < 49)
      {
	coord = this->to_tab(x, y, i);
	if (coord >= 0 && coord <= 361)
	  map.at(coord).mapB |= mask;
	mask <<= 1;
	map.at(coord).status = 0;
	++i;
      }
  else
    while (i < 49)
      {
	coord = this->to_tab(x, y, i);
	if (coord >= 0 && coord <= 361)
	  map.at(coord).mapN |= mask;
	mask <<= 1;
	map.at(coord).status = 1;
	++i;
      }
}

void	Arbitre::erase_twice(std::unordered_map<int, struct s_t> &map,
			     int x1, int y1, int x2, int y2, bool player)
{
  std::bitset<49>	mask(std::string(M_MOD_MAP));
  int			i = 0;
  int			coord;
  int			coord2;

  if (player == PLAYER_BLANC)
    while (i < 49)
      {
	coord = this->to_tab(x1, y1, i);
	coord2 = this->to_tab(x2, y2, i);
	if (coord >= 0 && coord <= 361)
	  map.at(coord).mapB = (map.at(coord).mapB & mask) ^ mask;
	if (coord2 >= 0 && coord2 <= 361)
	map.at(coord2).mapB = (map.at(coord2).mapB & mask) ^ mask;
	++i;
	mask <<= 1;
      }
  else
    while (i < 49)
      {
	coord = this->to_tab(x1, y1, i);
	coord2 = this->to_tab(x2, y2, i);
	if (coord >= 0 && coord <= 361)
	  map.at(coord).mapN = (map.at(coord).mapN & mask) ^ mask;
	if (coord2 >= 0 && coord2 <= 361)
	  map.at(coord2).mapN = (map.at(coord2).mapN & mask) ^ mask;
	++i;
	mask <<= 1;
      }
}

void	Arbitre::prise_pierre(std::bitset<49> &A, std::bitset<49> &B, int x, int y,
			      std::unordered_map<int, struct s_t> &map, bool player)
{
  if ((std::bitset<49>(A & this->masks.at(M_PP_E_A)) == this->masks.at(M_PP_E_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_E_B)) == this->masks.at(M_PP_E_B)))
    this->erase_twice(map, x, y + 1, x, y + 2, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_SE_A)) == this->masks.at(M_PP_SE_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_SE_B)) == this->masks.at(M_PP_SE_B)))
    this->erase_twice(map, x + 1, y + 1, x + 2, y + 2, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_S_A)) == this->masks.at(M_PP_S_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_S_B)) == this->masks.at(M_PP_S_B)))
    this->erase_twice(map, x + 1, y, x + 2, y, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_SW_A)) == this->masks.at(M_PP_SW_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_SW_B)) == this->masks.at(M_PP_SW_B)))
    this->erase_twice(map, x + 1, y - 1, x + 2, y - 2, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_W_A)) == this->masks.at(M_PP_W_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_W_B)) == this->masks.at(M_PP_W_B)))
    this->erase_twice(map, x, y - 1, x, y - 2, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_NW_A)) == this->masks.at(M_PP_NW_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_NW_B)) == this->masks.at(M_PP_NW_B)))
    this->erase_twice(map, x - 1, y - 1, x - 2, y - 2, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_N_A)) == this->masks.at(M_PP_N_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_N_B)) == this->masks.at(M_PP_N_B)))
    this->erase_twice(map, x - 1, y, x - 2, y, player);
  if ((std::bitset<49>(A & this->masks.at(M_PP_NE_A)) == this->masks.at(M_PP_NE_A)) &&
      (std::bitset<49>(B & this->masks.at(M_PP_NE_B)) == this->masks.at(M_PP_NE_B)))
    this->erase_twice(map, x - 1, y + 1, x - 2, y + 2, player);
}

bool	Arbitre::is_breakable(std::bitset<49> &mapA, std::bitset<49> &mapB)
{
  if (std::bitset<49>(mapA & this->masks.at(M_P1_E)) == this->masks.at(M_P1_E))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_W)) == this->masks.at(M_P1_W) &&
	  (mapB & this->masks.at(M_P2_E)) == false && (mapB & this->masks.at(M_P2_E)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_E)) == this->masks.at(M_P2_E) &&
	    (mapB & this->masks.at(M_P1_W)) == false && (mapB & this->masks.at(M_P1_W)) == false)
	  return (true);
    }
  if (std::bitset<49>(mapA & this->masks.at(M_P1_W)) == this->masks.at(M_P1_W))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_E)) == this->masks.at(M_P1_E) &&
	  (mapB & this->masks.at(M_P2_W)) == false && (mapB & this->masks.at(M_P2_W)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_W)) == this->masks.at(M_P2_W) &&
	    (mapB & this->masks.at(M_P1_E)) == false && (mapB & this->masks.at(M_P1_E)) == false)
	  return (true);
    }

  if (std::bitset<49>(mapA & this->masks.at(M_P1_N)) == this->masks.at(M_P1_N))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_S)) == this->masks.at(M_P1_S) &&
	  (mapB & this->masks.at(M_P2_N)) == false && (mapB & this->masks.at(M_P2_N)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_N)) == this->masks.at(M_P2_N) &&
	    (mapB & this->masks.at(M_P1_S)) == false && (mapB & this->masks.at(M_P1_S)) == false)
	  return (true);
    }
  if (std::bitset<49>(mapA & this->masks.at(M_P1_S)) == this->masks.at(M_P1_S))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_N)) == this->masks.at(M_P1_N) &&
	  (mapB & this->masks.at(M_P2_S)) == false && (mapB & this->masks.at(M_P2_S)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_S)) == this->masks.at(M_P2_S) &&
	    (mapB & this->masks.at(M_P1_N)) == false && (mapB & this->masks.at(M_P1_N)) == false)
	  return (true);
    }

  if (std::bitset<49>(mapA & this->masks.at(M_P1_NE)) == this->masks.at(M_P1_NE))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_SW)) == this->masks.at(M_P1_SW) &&
	  (mapB & this->masks.at(M_P2_NE)) == false && (mapB & this->masks.at(M_P2_NE)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_NE)) == this->masks.at(M_P2_NE) &&
	    (mapB & this->masks.at(M_P1_SW)) == false && (mapB & this->masks.at(M_P1_SW)) == false)
	  return (true);
    }
  if (std::bitset<49>(mapA & this->masks.at(M_P1_SW)) == this->masks.at(M_P1_SW))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_NE)) == this->masks.at(M_P1_NE) &&
	  (mapB & this->masks.at(M_P2_SW)) == false && (mapB & this->masks.at(M_P2_SW)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_SW)) == this->masks.at(M_P2_SW) &&
	    (mapB & this->masks.at(M_P1_NE)) == false && (mapB & this->masks.at(M_P1_NE)) == false)
	  return (true);
    }

  if (std::bitset<49>(mapA & this->masks.at(M_P1_NW)) == this->masks.at(M_P1_NW))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_SE)) == this->masks.at(M_P1_SE) &&
	  (mapB & this->masks.at(M_P2_NW)) == false && (mapB & this->masks.at(M_P2_NW)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_NW)) == this->masks.at(M_P2_NW) &&
	    (mapB & this->masks.at(M_P1_SE)) == false && (mapB & this->masks.at(M_P1_SE)) == false)
	  return (true);
    }
  if (std::bitset<49>(mapA & this->masks.at(M_P1_SE)) == this->masks.at(M_P1_SE))
    {
      if (std::bitset<49>(mapB & this->masks.at(M_P1_NW)) == this->masks.at(M_P1_NW) &&
	  (mapB & this->masks.at(M_P2_SE)) == false && (mapB & this->masks.at(M_P2_SE)) == false)
	return (true);
      else
	if (std::bitset<49>(mapB & this->masks.at(M_P2_SE)) == this->masks.at(M_P2_SE) &&
	    (mapB & this->masks.at(M_P1_NW)) == false && (mapB & this->masks.at(M_P1_NW)) == false)
	  return (true);
    }
  return (false);
}

bool	Arbitre::mass_breakable(std::unordered_map<int, struct s_t> &map, int coord_0, int coord_1, int coord_2, int coord_3, bool player)
{
  if (player == PLAYER_BLANC)
    {
      return (this->is_breakable(map.at(coord_0).mapB, map.at(coord_0).mapN) |
	      this->is_breakable(map.at(coord_1).mapB, map.at(coord_1).mapN) |
	      this->is_breakable(map.at(coord_2).mapB, map.at(coord_2).mapN) |
	      this->is_breakable(map.at(coord_3).mapB, map.at(coord_3).mapN));
    } 
  return (this->is_breakable(map.at(coord_0).mapN, map.at(coord_0).mapB) |
	  this->is_breakable(map.at(coord_1).mapN, map.at(coord_1).mapB) |
	  this->is_breakable(map.at(coord_2).mapN, map.at(coord_2).mapB) |
	  this->is_breakable(map.at(coord_3).mapN, map.at(coord_3).mapB));
}
 
bool	Arbitre::cinq_cassable(std::unordered_map<int, struct s_t> &map, int coord, int x, int y, bool player)
{
  int			winflag = 0;
  std::bitset<49>	mapA;
  std::bitset<49>	mapB;

  if (player == PLAYER_BLANC)		     
    {
      mapA = map.at(coord).mapB;
      mapB = map.at(coord).mapN;
    }
  else
    {
      mapA = map.at(coord).mapN;
      mapB = map.at(coord).mapB;
    }

  if (std::bitset<49>(mapA & this->masks.at(M_C2_H_A)) == this->masks.at(M_C2_H_A))
    if (this->mass_breakable(map, coord - 2, coord - 1, coord + 1, coord + 2, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C2_HV_A)) == this->masks.at(M_C2_HV_A))
    if (this->mass_breakable(map, coord - 40, coord - 20, coord + 20, coord + 40, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C2_V_A)) == this->masks.at(M_C2_V_A))
    if (this->mass_breakable(map, coord - 38, coord - 19, coord + 19, coord + 38, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C2_VH_A)) == this->masks.at(M_C2_VH_A))
    if (this->mass_breakable(map, coord - 36, coord - 18, coord + 18, coord + 36, player))
      return (false);

  if (std::bitset<49>(mapA & this->masks.at(M_C1_E_A)) == this->masks.at(M_C1_E_A))
    if (this->mass_breakable(map, coord - 1, coord + 1, coord + 2, coord + 3, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_SE_A)) == this->masks.at(M_C1_SE_A))
    if (this->mass_breakable(map, coord - 20, coord + 20, coord + 40, coord + 60, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_S_A)) == this->masks.at(M_C1_S_A))
    if (this->mass_breakable(map, coord - 19, coord + 19, coord + 38, coord + 57, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_SW_A)) == this->masks.at(M_C1_SW_A))
    if (this->mass_breakable(map, coord - 18, coord + 18, coord + 36, coord + 54, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_W_A)) == this->masks.at(M_C1_W_A))
    if (this->mass_breakable(map, coord - 3, coord - 2, coord - 1, coord + 1, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_NW_A)) == this->masks.at(M_C1_NW_A))
    if (this->mass_breakable(map, coord - 60, coord - 40, coord - 20, coord + 20, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_N_A)) == this->masks.at(M_C1_N_A))
    if (this->mass_breakable(map, coord - 57, coord - 38, coord - 19, coord + 19, player))
      return (false);
  if (std::bitset<49>(mapA & this->masks.at(M_C1_NE_A)) == this->masks.at(M_C1_NE_A))
    if (this->mass_breakable(map, coord - 54, coord - 36, coord - 18, coord + 18, player))
      return (false);

  if (player == PLAYER_BLANC)
    {
      if (std::bitset<49>(mapA & this->masks.at(M_C0_E_A)) == this->masks.at(M_C0_E_A) &&
	  std::bitset<49>(map.at(coord + 3).mapB & this->masks.at(M_P1_E)) == this->masks.at(M_P1_E))
	if (this->mass_breakable(map, coord + 1, coord + 2, coord + 3, coord + 4, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_SE_A)) == this->masks.at(M_C0_SE_A) &&
	  std::bitset<49>(map.at(coord + 60).mapB & this->masks.at(M_P1_SE)) == this->masks.at(M_P1_SE))
	if (this->mass_breakable(map, coord + 20, coord + 40, coord + 60, coord + 80, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_S_A)) == this->masks.at(M_C0_S_A) &&
	  std::bitset<49>(map.at(coord + 57).mapB & this->masks.at(M_P1_S)) == this->masks.at(M_P1_S))
	if (this->mass_breakable(map, coord + 19, coord + 38, coord + 57, coord + 76, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_SW_A)) == this->masks.at(M_C0_SW_A) &&
	  std::bitset<49>(map.at(coord + 54).mapB & this->masks.at(M_P1_SW)) == this->masks.at(M_P1_SW))
	if (this->mass_breakable(map, coord + 18, coord + 36, coord + 54, coord + 72, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_W_A)) == this->masks.at(M_C0_W_A) &&
	  std::bitset<49>(map.at(coord - 3).mapB & this->masks.at(M_P1_W)) == this->masks.at(M_P1_W))
	if (this->mass_breakable(map, coord - 1, coord - 2, coord - 3, coord - 4, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_NW_A)) == this->masks.at(M_C0_NW_A) &&
	  std::bitset<49>(map.at(coord - 60).mapB & this->masks.at(M_P1_NW)) == this->masks.at(M_P1_NW))
	if (this->mass_breakable(map, coord - 20, coord - 40, coord - 60, coord - 80, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_N_A)) == this->masks.at(M_C0_N_A) &&
	  std::bitset<49>(map.at(coord - 57).mapB & this->masks.at(M_P1_N)) == this->masks.at(M_P1_N))
	if (this->mass_breakable(map, coord - 19, coord - 38, coord - 57, coord - 76, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_NE_A)) == this->masks.at(M_C0_NE_A) &&
	  std::bitset<49>(map.at(coord - 54).mapB & this->masks.at(M_P1_NE)) == this->masks.at(M_P1_NE))
	if (this->mass_breakable(map, coord - 18, coord - 36, coord - 54, coord - 72, player))
	  return (false);
    }
  else
    {
      if (std::bitset<49>(mapA & this->masks.at(M_C0_E_A)) == this->masks.at(M_C0_E_A) &&
	  std::bitset<49>(map.at(coord + 3).mapN & this->masks.at(M_P1_E)) == this->masks.at(M_P1_E))
	if (this->mass_breakable(map, coord + 1, coord + 2, coord + 3, coord + 4, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_SE_A)) == this->masks.at(M_C0_SE_A) &&
	  std::bitset<49>(map.at(coord + 60).mapN & this->masks.at(M_P1_SE)) == this->masks.at(M_P1_SE))
	if (this->mass_breakable(map, coord + 20, coord + 40, coord + 60, coord + 80, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_S_A)) == this->masks.at(M_C0_S_A) &&
	  std::bitset<49>(map.at(coord + 57).mapN & this->masks.at(M_P1_S)) == this->masks.at(M_P1_S))
	if (this->mass_breakable(map, coord + 19, coord + 38, coord + 57, coord + 76, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_SW_A)) == this->masks.at(M_C0_SW_A) &&
	  std::bitset<49>(map.at(coord + 54).mapN & this->masks.at(M_P1_SW)) == this->masks.at(M_P1_SW))
	if (this->mass_breakable(map, coord + 18, coord + 36, coord + 54, coord + 72, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_W_A)) == this->masks.at(M_C0_W_A) &&
	  std::bitset<49>(map.at(coord - 3).mapN & this->masks.at(M_P1_W)) == this->masks.at(M_P1_W))
	if (this->mass_breakable(map, coord - 1, coord - 2, coord - 3, coord - 4, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_NW_A)) == this->masks.at(M_C0_NW_A) &&
	  std::bitset<49>(map.at(coord - 60).mapN & this->masks.at(M_P1_NW)) == this->masks.at(M_P1_NW))
	if (this->mass_breakable(map, coord - 20, coord - 40, coord - 60, coord - 80, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_N_A)) == this->masks.at(M_C0_N_A) &&
	  std::bitset<49>(map.at(coord - 57).mapN & this->masks.at(M_P1_N)) == this->masks.at(M_P1_N))
	if (this->mass_breakable(map, coord - 19, coord - 38, coord - 57, coord - 76, player))
	  return (false);
      if (std::bitset<49>(mapA & this->masks.at(M_C0_NE_A)) == this->masks.at(M_C0_NE_A) &&
	  std::bitset<49>(map.at(coord - 54).mapN & this->masks.at(M_P1_NE)) == this->masks.at(M_P1_NE))
	if (this->mass_breakable(map, coord - 18, coord - 36, coord - 54, coord - 72, player))
	  return (false);
    }
  return (true);
}


bool	Arbitre::double_trois(std::unordered_map<int, struct s_t> &map, int coord, int x, int y, bool player)
{
  // en construction
  return (true);
}

bool	Arbitre::basic_tests(std::bitset<49> &mapB, std::bitset<49> &mapN)
{
  if ((std::bitset<49>(mapB & this->masks.at(M_BT_A)) == this->masks.at(M_BT_A)) ||
      (std::bitset<49>(mapN & this->masks.at(M_BT_A)) == this->masks.at(M_BT_A)))
    return (false);
  return (true);
}

int	Arbitre::test_move(std::unordered_map<int, struct s_t> &map, int x, int y, int player)
{
  int	coord = x * 19 + y;

  if (this->basic_tests(map[coord].mapB, map[coord].mapN) &&
      this->double_trois(map, coord, x, y, player))
    {
      this->prise_pierre(map[coord].mapB, map[coord].mapN, x, y, map, player);
      //if (this->cinq_cassable(map, coord, x, y, player))
      //return (GAME_END);
    }
  else
    return (MOVE_NOT_OK);
  this->add_stone(map, x, y, player);
  return (MOVE_OK);
}
