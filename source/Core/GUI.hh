#ifndef GUI_HH_
#define GUI_HH_

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <bitset>
#include <unordered_map>
//#include "Core.hpp"

#define FONT_BEBAS_PATH		"font/BEBAS.tff"
#define FONT_OSTRICH_PATH	"fonts/ostrich-regular.tff"

#define MUSIC_MENU_PATH		"music/menu.ogg"
#define SOUND_BUTTON_PATH	"sound/button.ogg"

class		GUI
{
private:
  int	x;
  int	y;
  int	i;
  int	j;

  sf::ContextSettings	settings;
  sf::Texture	        t_border_t;
  sf::Texture	        t_border_r;
  sf::Texture	        t_border_b;
  sf::Texture	        t_border_l;
  sf::Texture	        t_corner_br;
  sf::Texture	        t_corner_bl;
  sf::Texture	        t_corner_tr;
  sf::Texture	        t_corner_tl;
  sf::Texture	        t_middle;
  sf::Texture	        t_stone_w;
  sf::Texture	        t_stone_b;
  sf::RectangleShape	*sh_border_t;
  sf::RectangleShape	*sh_border_r;
  sf::RectangleShape	*sh_border_l;
  sf::RectangleShape	*sh_border_b;
  sf::RectangleShape	*sh_corner_br;
  sf::RectangleShape	*sh_corner_bl;
  sf::RectangleShape	*sh_corner_tr;
  sf::RectangleShape	*sh_corner_tl;
  sf::RectangleShape	*sh_middle;
  sf::RectangleShape	*sh_stone_w;
  sf::RectangleShape	*sh_stone_b;
  sf::Sprite		sp_border_b;
  sf::Sprite		sp_border_l;
  sf::Sprite		sp_border_r;
  sf::Sprite		sp_border_t;
  sf::Sprite		sp_corner_br;
  sf::Sprite		sp_corner_bl;
  sf::Sprite		sp_corner_tr;
  sf::Sprite		sp_corner_tl;
  sf::Sprite		sp_middle;
  sf::Sprite		sp_stone_w;
  sf::Sprite		sp_stone_b;
  sf::RenderWindow	*GameWindow;

public:

  GUI();
  ~GUI();

  sf::Vector2i	getEvent(int player);
  void		affStone(int, int, int, sf::Vector2i);
  void		affMap();
  int		menuLaunch();
  void		updateMap(std::unordered_map<int, struct s_t> &map);
  void		setX(int _x);
  void		setY(int _y);
  void		setI(int _i);
  void		setJ(int _j);
  int		getX() const;
  int		getY() const;
  int		getI() const;
  int		getJ() const;
  sf::RenderWindow	*getWin();
};

#endif /* !GUI_HH_ */
