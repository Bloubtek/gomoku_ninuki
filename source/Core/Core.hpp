//
// Core.hpp for gomoku_ninuki in /home/bloubtek/projet/gomoku_ninuki/source/Core
//
// Made by
// Login   <bloubtek@epitech.net>
//
// Started on  Tue Nov 24 14:12:21 2015
// Last update Wed Nov 25 14:34:33 2015 
//

#ifndef CORE_HPP_
#define CORE_HPP_

#include <bitset>
#include <unordered_map>
#include <list>
//#include "Player.hpp"
//#include "HPlayer.hpp"
#include "IaPlayer.hpp"
#include "GUI.hh"
#include "../arbitre/arbitre.hpp"
#include <string>
#include <iostream>
#define BLANC_COLOR	0xFFFAF0
#define BLUE_COLOR	0x1E90FF

/*
struct  s_t
{
  std::bitset<49>	mapB;
  std::bitset<49>	mapN;
  std::bitset<49>	Walls;
  int		status;
};
*/

class	Core
{
private:
  Arbitre	arbitre;
  GUI		interface;

  std::list<Player>	players;
  int		turn; // faire %2 pour savoir if P1turn ou P2turn
  std::unordered_map<int, struct s_t>	map;

  public:

  Core();
  ~Core();

  //  GUI	getInterface();
  void Launch();
  bool	PvpLoop();
  bool	IaLoop();
};

#endif /* !CORE_HPP */
