//
// Player.hpp for gomoku_ninuki in /home/bloubtek/projet/gomoku_ninuki/source/Core
//
// Made by
// Login   <bloubtek@epitech.net>
//
// Started on  Wed Nov 25 14:31:02 2015
// Last update Wed Nov 25 14:32:24 2015 
//

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>

class	Player
{

protected:

  std::string	_name;
  int		_color; //couleur du joueur en hexa
  int		_stonesInMap; // nb de pierres que le joueur a sur le plateau
  bool		_played; //dit si le joueur a joué ce tour-ci ou non

public:

  Player();
  ~Player();

  int		getColor();
  int		getNbStones();
  void		setColor(int color);
  void		addStone();
  std::string	getName();
  void		setName(std::string name);
};

#endif /* !PLAYER_HPP */
