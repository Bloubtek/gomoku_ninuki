//
// main.cpp for test in /home/vincen_r/rendu/gomoku_ninuki/source/GUI
//
// Made by Romain Vincent
// Login   <vincen_r@epitech.net>
//
// Started on  Mon Nov  9 14:13:39 2015 Romain Vincent
// Last update Mon Jan 25 11:44:37 2016 Avice
//

#include <iostream>
#include "GUI.hh"
#include "Core.hpp"

#define SIZE_W 950
#define SIZE_C SIZE_W/19
#define CASE_O (SIZE_W - (SIZE_C * 19)) / 2
#define PATH_ASSET_BORDER_B "assets/tile_border_B.png"
#define PATH_ASSET_BORDER_T "assets/tile_border_T.png"
#define PATH_ASSET_BORDER_L "assets/tile_border_L.png"
#define PATH_ASSET_BORDER_R "assets/tile_border_R.png"
#define PATH_ASSET_CORNER_TR "assets/tile_corner_TR.png"
#define PATH_ASSET_CORNER_TL "assets/tile_corner_TL.png"
#define PATH_ASSET_CORNER_BR "assets/tile_corner_BR.png"
#define PATH_ASSET_CORNER_BL "assets/tile_corner_BL.png"
#define PATH_ASSET_MIDDLE "assets/tile_middle.png"
#define PATH_ASSET_STONE_B "assets/tile_stone_B.png"
#define PATH_ASSET_STONE_W "assets/tile_stone_W.png"

GUI::GUI()
{
  this->sh_border_b = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_border_t = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_border_l = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_border_r = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_corner_br = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_corner_bl = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_corner_tl = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_corner_tr = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_middle = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_stone_b = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->sh_stone_w = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));

  this->settings.antialiasingLevel = 8;

  GameWindow = new sf::RenderWindow(sf::VideoMode(SIZE_W, SIZE_W), "SFML shapes", sf::Style::Close, this->settings);

  if (!this->t_border_b.loadFromFile(PATH_ASSET_BORDER_B))
    std::cerr << "Error while loading border bottom skin!" << std::endl;
  if (!this->t_border_l.loadFromFile(PATH_ASSET_BORDER_L))
    std::cerr << "Error while loading border left skin!" << std::endl;
  if (!this->t_border_r.loadFromFile(PATH_ASSET_BORDER_R))
    std::cerr << "Error while loading border right skin!" << std::endl;
  if (!this->t_border_t.loadFromFile(PATH_ASSET_BORDER_T))
    std::cerr << "Error while loading border top skin!" << std::endl;
  if (!this->t_corner_br.loadFromFile(PATH_ASSET_CORNER_BR))
    std::cerr << "Error while loading corner bottom right skin!" << std::endl;
  if (!this->t_corner_bl.loadFromFile(PATH_ASSET_CORNER_BL))
    std::cerr << "Error while loading corner bottom left skin!" << std::endl;
  if (!this->t_corner_tl.loadFromFile(PATH_ASSET_CORNER_TL))
    std::cerr << "Error while loading corner top left skin!" << std::endl;
  if (!this->t_corner_tr.loadFromFile(PATH_ASSET_CORNER_TR))
    std::cerr << "Error while loading corner top rightskin!" << std::endl;
  if (!this->t_middle.loadFromFile(PATH_ASSET_MIDDLE))
    std::cerr << "Error while loading center skin!" << std::endl;
  if (!this->t_stone_w.loadFromFile(PATH_ASSET_STONE_W))
    std::cerr << "Error while loading white stone skin!" << std::endl;
  if (!this->t_stone_b.loadFromFile(PATH_ASSET_STONE_B))
    std::cerr << "Error while loading black stone skin!" << std::endl;

  this->x = 0;
  this->y = 0;

  this->sp_border_l.setTexture(this->t_border_l);
  this->sp_border_t.setTexture(this->t_border_t);
  this->sp_border_r.setTexture(this->t_border_r);
  this->sp_border_b.setTexture(this->t_border_b);
  this->sp_corner_tr.setTexture(this->t_corner_tr);
  this->sp_corner_br.setTexture(this->t_corner_br);
  this->sp_corner_tl.setTexture(this->t_corner_tl);
  this->sp_corner_bl.setTexture(this->t_corner_bl);
  this->sp_middle.setTexture(this->t_middle);
  this->sp_stone_w.setTexture(this->t_stone_w);
  this->sp_stone_b.setTexture(this->t_stone_b);

  this->sh_border_l->setTexture(&this->t_border_l);
  this->sh_border_t->setTexture(&this->t_border_t);
  this->sh_border_r->setTexture(&this->t_border_r);
  this->sh_border_b->setTexture(&this->t_border_b);
  this->sh_corner_br->setTexture(&this->t_corner_br);
  this->sh_corner_bl->setTexture(&this->t_corner_bl);
  this->sh_corner_tr->setTexture(&this->t_corner_tr);
  this->sh_corner_tl->setTexture(&this->t_corner_tl);
  this->sh_middle->setTexture(&this->t_middle);
  this->sh_stone_w->setTexture(&this->t_stone_w);
  this->sh_stone_b->setTexture(&this->t_stone_b);

  this->t_border_l.setSmooth(true);
  this->t_border_r.setSmooth(true);
  this->t_border_t.setSmooth(true);
  this->t_border_b.setSmooth(true);
  this->t_corner_br.setSmooth(true);
  this->t_corner_bl.setSmooth(true);
  this->t_corner_tr.setSmooth(true);
  this->t_corner_tl.setSmooth(true);
  this->t_middle.setSmooth(true);
  this->t_stone_w.setSmooth(true);
  this->t_stone_b.setSmooth(true);

  //  GameWindow->clear(sf::Color::Black);
}

GUI::~GUI()
{

}

void			GUI::affMap()
{
     while (this->x <= SIZE_W && this->y <= SIZE_W)
	{
	  if (this->x == 0 && this->y == 0)
	    {
	      this->sh_corner_tl->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_corner_tl);
	    }
	  if (this->x == (SIZE_C * 18) && this->y == 0)
	    {
	      this->sh_corner_tr->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_corner_tr);
	    }
	  if (this->x == 0 && this->y == (SIZE_C * 18))
	    {
	      this->sh_corner_bl->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_corner_bl);
	    }
	  if (this->x == (SIZE_C * 18) && this->y == (SIZE_C * 18))
	    {
	      this->sh_corner_br->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_corner_br);
	    }
	  if (this->x > 0 && this-> x < (SIZE_C * 18) && this->y == 0)
	    {
	      this->sh_border_t->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_border_t);
	    }
	  if (this->x == 0 && this->y > 0 && this->y < (SIZE_C * 18))
	    {
	      this->sh_border_l->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_border_l);
	    }
	  if (this->x > 0 && this-> x < (SIZE_C * 18) && this->y == (SIZE_C * 18))
	    {
	      this->sh_border_b->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_border_b);
	    }
	  if (this->x == (SIZE_C * 18) && this->y > 0 && this->y < (SIZE_C * 18))
	    {
	      this->sh_border_r->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_border_r);
	    }
	  if (this->x > 0 && this->x < SIZE_C * 18 && this->y > 0 && this->y < SIZE_C * 18)
	    {
	      this->sh_middle->setPosition(this->x, this->y);
	      GameWindow->draw(*this->sh_middle);
	    }
	  this->x += SIZE_C;
	  if (this->x == SIZE_C * 19)
	    {
	      this->x = 0;
	      this->y += SIZE_C;
	    }
	}
      GameWindow->display();
      return;
}

sf::Vector2i		GUI::getEvent(int player)
{
  sf::Event event;
  sf::Vector2i	pos;

  affMap();
  while (GameWindow->waitEvent(event))
    {
      if (event.type == sf::Event::Closed)
	{
	  GameWindow->close();
	  exit(0);
	}
      if (event.type == sf::Event::MouseButtonPressed)
	{
	  if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	    {
	      pos = sf::Mouse::getPosition(*GameWindow);
	      return pos;
	    }
	}
      /*      if (event.type == sf::Event::KeyPressed)
	{
	  if (event.key.code == sf::Keyboard::Left)
	    std::cout<< "Left key pressed" << std::endl;
	  if (event.key.code == sf::Keyboard::Right)
	    std::cout<< "Right key pressed" << std::endl;
	  if (event.key.code == sf::Keyboard::Up)
	    std::cout<< "Up key pressed" << std::endl;
	  if (event.key.code == sf::Keyboard::Down)
	    std::cout<< "Down key pressed" << std::endl;
	  if (event.key.code == sf::Keyboard::Return)
	    std::cout<< "Enter key pressed" << std::endl;
	    }*/
    }
  //  affMap();
  return (pos);
}

void		GUI::affStone(int x, int y, int player, sf::Vector2i pos)
{
  if (player == 0)
    {
      this->sh_stone_w->setPosition((pos.x / 50) * 50, (pos.y / 50) * 50);
      GameWindow->draw(*this->sh_stone_w);
      GameWindow->display();
    }
    else if (player == 1)
    {
      this->sh_stone_b->setPosition((pos.x / 50) * 50, (pos.y / 50) * 50);
      GameWindow->draw(*this->sh_stone_b);
      GameWindow->display();
    }
  return;
}

void		GUI::updateMap(std::unordered_map<int, struct s_t> &map)
{
  int	i = 0;
  sf::Vector2i	pos;

  pos.x = 0;
  pos.y = 0;
  //GameWindow->clear();
  affMap();
  while (i < 361)
    {
      affStone(0, 0, (map.at(i)).status, pos);
      if (pos.x == 19)
	{
	  pos.x = 0;
	  y += 1;
	}
      ++pos.x;
      ++i;
    }
}

void		GUI::setX(int _x)
{
  this->x = _x;
}

void		GUI::setY(int _y)
{
  this->y = _y;
}

void		GUI::setI(int _i)
{
  this->i = _i;
}

void		GUI::setJ(int _j)
{
  this->j = _j;
}

int		GUI::getX() const
{
  return (this->x);
}

int		GUI::getY() const
{
  return (this->y);
}

int		GUI::getI() const
{
  return (this->i);
}

int		GUI::getJ() const
{
  return (this->j);
}

sf::RenderWindow	*GUI::getWin()
{
  return (this->GameWindow);
}

int			GUI::menuLaunch()
{
  sf::RenderWindow	window(sf::VideoMode(800, 600), "Gomoku");
  sf::Event		event;
  sf::Font		font_bebas, font_ostrich;
  sf::Music		music;
  sf::SoundBuffer	soundBuffer_button;
  sf::Sound		sound_button;
  bool			play = true;

  if (!font_bebas.loadFromFile("fonts/BEBAS.ttf"))
    return false;
  if (!font_ostrich.loadFromFile("fonts/ostrich-regular.ttf"))
    return false;

  sf::Text title("Gomoku", font_bebas, 75);
  title.setColor(sf::Color(128,128,0));
  title.move(270.f,100.f);

  sf::Text BTN_1("Joueur contre IA", font_ostrich, 50);
  BTN_1.setColor(sf::Color(0,128,0));
  BTN_1.move(230.f,280.f);


  sf::Text BTN_2("Joueur contre Joueur", font_ostrich, 50);
  BTN_2.setColor(sf::Color(128,128,128));
  BTN_2.move(210.f,350.f);

  sf::Text BTN_3("Quitter", font_ostrich, 50);
  BTN_3.setColor(sf::Color(128,128,128));
  BTN_3.move(328.f,420.f);

  if (!soundBuffer_button.loadFromFile(SOUND_BUTTON_PATH))
    return false;
  sound_button.setBuffer(soundBuffer_button);

  if (!music.openFromFile(MUSIC_MENU_PATH))
    return false;
  music.play();

  window.setFramerateLimit(15);

  while (window.isOpen())
  {
    while (window.pollEvent(event))
      {
	if (event.type == sf::Event::Closed)
	  window.close();
	else
	  {
	    if (event.type == sf::Event::KeyPressed)
	      {
		if (event.key.code == sf::Keyboard::Escape)
		  window.close();
		if (event.key.code == sf::Keyboard::Return)
		  {
		    window.close();
		    music.stop();
		    if (BTN_1.getColor() == sf::Color(0,128,0))
		      return (1);
		    else if (BTN_2.getColor() == sf::Color(0,128,0))
		      return (0);
		    else if (BTN_3.getColor() == sf::Color(0,128,0))
		      return (2);
		  }
		if (event.key.code == sf::Keyboard::Down)
		  {
		    if (play)
		      {
			if (BTN_1.getColor() == sf::Color(128,128,128)
			    && BTN_2.getColor() == sf::Color(0,128,0))
			  {
			    play = false;
			    BTN_1.setColor(sf::Color(128,128,128));
			    BTN_2.setColor(sf::Color(128,128,128));
			    BTN_3.setColor(sf::Color(0,128,0));
			  }
			else if (BTN_1.getColor() == sf::Color(0,128,0)
				 && BTN_2.getColor() == sf::Color(128,128,128))
			  {
			    BTN_1.setColor(sf::Color(128,128,128));
			    BTN_2.setColor(sf::Color(0,128,0));
			    BTN_3.setColor(sf::Color(128,128,128));
			  }
			sound_button.play();
		      }
		  }
		if (event.key.code == sf::Keyboard::Up)
		  {
		    if (!play)
		      {

			if (BTN_2.getColor() == sf::Color(0,128,0)
			    && BTN_3.getColor() == sf::Color(128,128,128))
			  {
			    play = true;
			    BTN_1.setColor(sf::Color(0,128,0));
			    BTN_2.setColor(sf::Color(128,128,128));
			    BTN_3.setColor(sf::Color(128,128,128));
			  }
			else if (BTN_2.getColor() == sf::Color(128,128,128)
				 && BTN_3.getColor() == sf::Color(0,128,0))
			  {
			    BTN_1.setColor(sf::Color(128,128,128));
			    BTN_2.setColor(sf::Color(0,128,0));
			    BTN_3.setColor(sf::Color(128,128,128));
			  }
			sound_button.play();
		      }
		  }
	      }
	  }
      }
    if (music.getStatus() == 0)
      music.play();

    //    title.setColor(sf::Color(sf::Randomizer::Random(0,128), sf::Randomizer::Random(0,128),0));

    window.clear();
    window.draw(title);
    window.draw(BTN_1);
    window.draw(BTN_2);
    window.draw(BTN_3);
    window.display();
  }
  return 2;
}
