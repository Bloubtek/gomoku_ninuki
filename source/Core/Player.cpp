#include "Player.hpp"

Player::Player()
{
  this->_color = 0;
  this->_stonesInMap = 0;
  this->_played = false;
}

Player::~Player()
{

}

int	Player::getColor()
{
  return (this->_color);
}

int	Player::getNbStones()
{
  return (this->_stonesInMap);
}

void	Player::setColor(int color)
{
  this->_color = color;
}

void	Player::addStone()
{
  _stonesInMap += 1;
}

std::string	Player::getName()
{
  return (this->_name);
}

void	Player::setName(std::string name)
{
  this->_name = name;
}
