#include "HPlayer.hpp"

HPlayer::HPlayer() : Player()
{
    _color = 0;
    _stonesInMap = 0;
    _played = false;
}

HPlayer::HPlayer(std::string name, int color)
{
  _name = name;
  _color = color;
}

HPlayer::~HPlayer()
{

}
