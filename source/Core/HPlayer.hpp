#ifndef HPLAYER_HPP_
#define HPLAYER_HPP_

#include "Player.hpp"
class	HPlayer : public Player
{
public:

  HPlayer();
  HPlayer(std::string name, int color);
  ~HPlayer();
};

#endif /* HPLAYER_HPP_ */
