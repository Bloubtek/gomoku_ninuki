//
// menu.cpp for  in /home/jousse_g/rendu/gomoku_ninuki/source/menu
//
// Made by Gautier Jousset
// Login   <jousse_g@epitech.net>
//
// Started on  Tue Nov 24 21:15:35 2015 Gautier Jousset
// Last update Wed Nov 25 13:13:14 2015 
//

#include "menu.hpp"

bool			menu()
{
  sf::RenderWindow	window(sf::VideoMode(800, 600), "Gomoku");
  sf::Event		event;
  sf::Font		font_bebas, font_ostrich;
  sf::Music		music;
  sf::SoundBuffer	soundBuffer_button;
  sf::Sound		sound_button;
  bool			play = true;

  if (!font_bebas.LoadFromFile("fonts/BEBAS.ttf", 50))
    return false;
  if (!font_ostrich.LoadFromFile("fonts/ostrich-regular.ttf", 50))
    return false;

  sf::String title("Gomoku", font_bebas, 75);
  title.SetColor(sf::Color(128,128,0));
  title.Move(270.f,100.f);

  sf::String BTN_1("Jouer", font_ostrich, 50);
  BTN_1.SetColor(sf::Color(0,128,0));
  BTN_1.Move(340.f,280.f);

  sf::String BTN_2("Quitter", font_ostrich, 50);
  BTN_2.SetColor(sf::Color(128,128,128));
  BTN_2.Move(328.f,350.f);

  if (!soundBuffer_button.LoadFromFile(SOUND_BUTTON_PATH))
    return false;
  sound_button.SetBuffer(soundBuffer_button);

  if (!music.OpenFromFile(MUSIC_MENU_PATH))
    return false;
  music.Play();

  window.SetFramerateLimit(15);

  while (window.IsOpened())
  {
    while (window.GetEvent(event))
      {
	if (event.Type == sf::Event::Closed)
	  window.Close();
	else
	  {
	    if (event.Type == sf::Event::KeyPressed)
	      {
		if (event.Key.Code == sf::Key::Escape)
		  window.Close();
		if (event.Key.Code == sf::Key::Return)
		  {
		    window.Close();
		    music.Stop();
		    return play;
		  }
		if (event.Key.Code == sf::Key::Down)
		  {
		    if (play)
		      {
			play = false;
			BTN_1.SetColor(sf::Color(128,128,128));
			BTN_2.SetColor(sf::Color(0,128,0));
			sound_button.Play();
		      }
		  }
		if (event.Key.Code == sf::Key::Up)
		  {
		    if (!play)
		      {
			play = true;
			BTN_1.SetColor(sf::Color(0,128,0));
			BTN_2.SetColor(sf::Color(128,128,128));
			sound_button.Play();
		      }
		  }
	      }
	  }
      }
    if (music.GetStatus() == 0)
      music.Play();

    title.SetColor(sf::Color(sf::Randomizer::Random(0,128), sf::Randomizer::Random(0,128),0));

    window.Clear();
    window.Draw(title);
    window.Draw(BTN_1);
    window.Draw(BTN_2);
    window.Display();
  }
  return false;
}
