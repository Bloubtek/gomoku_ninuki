//
// IaPlayer.hpp for gomoku_ninuki in /home/bloubtek/projet/gomoku_ninuki/source/Core
//
// Made by
// Login   <bloubtek@epitech.net>
//
// Started on  Wed Nov 25 14:29:45 2015
// Last update Wed Nov 25 14:30:43 2015 
//

#ifndef IAPLAYER_HPP_
#define IAPLAYER_HPP_

#include "HPlayer.hpp"
class	IaPlayer : public Player
{
public:

  IaPlayer();
  IaPlayer(std::string name, int color);
  ~IaPlayer();
};

#endif /* !IAPLAYER_HPP_ */
