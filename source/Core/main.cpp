//
// main.cpp for gomoku_ninuki in /home/bloubtek/projet/gomoku_ninuki/source/Core
//
// Made by
// Login   <bloubtek@epitech.net>
//
// Started on  Mon Nov 23 16:52:14 2015
// Last update Mon Nov 23 16:52:59 2015 
//

#include "Core.hpp"

int	main()
{
  Core	*game = new Core();

  game->Launch();
}
