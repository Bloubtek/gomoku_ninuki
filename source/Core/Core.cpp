//
// Core.cpp for gomoku_ninuki in /home/bloubtek/projet/gomoku_ninuki/source/Core
//
// Made by
// Login   <bloubtek@epitech.net>
//
// Started on  Tue Nov 24 13:48:55 2015
// Last update Sat Jan 23 16:13:04 2016 Avice
//

#include "Core.hpp"

Core::Core()
{
  Player	*p1 = new HPlayer((std::string)"Joueur 1", BLANC_COLOR);
  int		i = 0;
  s_t		init;
  players.push_back(*p1);
  turn = 0;
  init.mapB.reset();
  init.mapN.reset();
  init.Walls.reset();
  init.status = 2;
  while (i <= 361)
    {
      this->map.emplace(i, init);
      ++i;
    }
}

Core::~Core()
{

}

void	Core::Launch()
{
  int	i;
  // le menu devra renvoyer true pour JCJ et false pour JCIA
  i = this->interface.menuLaunch();
  if (i == true)
    this->PvpLoop();
  //  else if (i == 0) //JCIA
    //this->PvpLoop();
 else if (i == 2)
   return;
}

// Les boucles de jeu (une pour le JCJ et une pour le JCIA)

bool	Core::PvpLoop()
{
  //this->Ia.~IAPlayer();
  Player *p2 = new HPlayer((std::string)"Joueur 2", BLUE_COLOR);
  sf::Vector2i	pos;
  int		move;

  this->players.push_back(*p2);
  while (interface.getWin()->isOpen())
   {
     pos = this->interface.getEvent((this->turn % 2));
     //add when finished
     //boucle infinie dans le add_stone ?
     std::cout << "avant add_stone, x = " << pos.x << " et y = " << pos.y << std::endl;
     if ((pos.x > 10 && pos.x < 940) && (pos.y > 10 && pos.y < 940))
       {
	 move = this->arbitre.test_move(this->map, (pos.x / 50), (pos.y / 50), (this->turn % 2));
	 std::cout << "après add_stone" << std::endl;
	 if (move == MOVE_OK)
	   {
	     this->interface.affStone(pos.x, pos.y, (this->turn % 2), pos);
	     this->turn += 1;//faire un %2 sur turn pour définir P1 ou P2 (donc P1 = 0 et P2 = 1)
	   }
	 else if (move == GAME_END)
	   {
	     std::cout << "Bravo, vous avez gagné" << std::endl;
	     //exit(0);
	   }
       }
     //this->interface.updateMap(this->map);
   }
  return (true);
}
/*

//voir quand la partie IA sera à coder
bool	Core::IaLoop()
{
this->P2.~Player();
}
*/
