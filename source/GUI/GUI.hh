#ifndef GUI_HH_
#define GUI_HH_

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#define FONT_BEBAS_PATH		"font/BEBAS.tff"
#define FONT_OSTRICH_PATH	"fonts/ostrich-regular.tff"

#define MUSIC_MENU_PATH		"music/menu.ogg"
#define SOUND_BUTTON_PATH	"sound/button.ogg"

class		GUI
{
private:
  int	x;
  int	y;
  int	i;
  int	j;

  sf::ContextSettings	settings;
  sf::Texture		texture;
  sf::Texture		texture2;
  sf::RectangleShape	*shape;
  sf::RectangleShape	*shape2;
  sf::Sprite		sprite;
  sf::Sprite		sprite2;
  sf::RenderWindow	*GameWindow;
public:
  GUI();
  ~GUI();
  int		launchWindow();
  bool		menuLaunch();
  void		setX(int _x);
  void		setY(int _y);
  void		setI(int _i);
  void		setJ(int _j);
  int		getX() const;
  int		getY() const;
  int		getI() const;
  int		getJ() const;
};

#endif /* !GUI_HH_ */
