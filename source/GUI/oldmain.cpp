//
// main.cpp for test in /home/vincen_r/rendu/gomoku_ninuki/source/GUI
//
// Made by Romain Vincent
// Login   <vincen_r@epitech.net>
//
// Started on  Mon Nov  9 14:13:39 2015 Romain Vincent
// Last update Tue Nov 24 07:21:47 2015 vincen_r
//

#include <iostream>
#include <SFML/Graphics.hpp>

int	main(int ac, char **av)
{
  int	x;
  int	y;

  int	i;
  int	j;

  sf::ContextSettings settings;
  sf::Texture texture;
  sf::Texture texture2;
  sf::RectangleShape shape(sf::Vector2f(100, 100));
  sf::RectangleShape shape2(sf::Vector2f(100, 100));
  sf::Sprite sprite;
  sf::Sprite sprite2;

  settings.antialiasingLevel = 8;

  sf::RenderWindow window(sf::VideoMode(1000, 1000), "SFML shapes", sf::Style::Default, settings);

  if (!texture.loadFromFile("skin/red.jpg"))
    std::cerr << "NIQUE TA MERE LE SKIN" << std::endl;

  if (!texture.loadFromFile("skin/red.jpg"))
    std::cerr << "NIQUE TA MAMAN LE SKIN !" << std::endl;

  if (!texture2.loadFromFile("skin/blue.jpg"))
    std::cerr << "NIQUE TA MERE LE SKIN 2" << std::endl;

  if (!texture2.loadFromFile("skin/blue.jpg"))
    std::cerr << "NIQUE TA MAMAN LE SKIN 2!" << std::endl;

  x = 0;
  y = 0;
  i = 100;
  j = 0;
  sprite.setTexture(texture);
  sprite2.setTexture(texture2);
  shape.setTexture(&texture);
  shape2.setTexture(&texture2);
  texture.setSmooth(true);
  texture2.setSmooth(true);

  window.clear(sf::Color::Black);
  while (window.isOpen())
    {
      sf::Event event;
      while (window.pollEvent(event))
        {
	  if (event.type == sf::Event::Closed)
	    window.close();
        }

      while (x <= 1000 && y <= 1000)
	{
	  shape.setPosition(x, y);
	  shape2.setPosition(i, j);
	  window.draw(shape);
	  window.draw(shape2);
	  x = x + 200;
	  i = i + 200;
	  if (x == 1000 || i == 1000)
	    {
	      y = y + 100;
	      j = j + 100;
	      if (((x / 100) % 2) == 0)
		{
		  x = 100;
		  i = 0;
		}
	      else
		{
		  x = 0;
		  i = 100;
		}
	    }
	}
      window.display();
    }
  return 0;
}
