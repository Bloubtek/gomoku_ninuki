//
// main.cpp for test in /home/vincen_r/rendu/gomoku_ninuki/source/GUI
//
// Made by Romain Vincent
// Login   <vincen_r@epitech.net>
//
// Started on  Mon Nov  9 14:13:39 2015 Romain Vincent
// Last update Wed Nov 25 16:26:20 2015 
//

#include <iostream>
#include "GUI.hh"

#define SIZE_W 800
#define SIZE_C SIZE_W/19
#define CASE_O (SIZE_W - (SIZE_C * 19)) / 2

GUI::GUI()
{
  this->shape = new sf::RectangleShape(sf::Vector2f(SIZE_C, SIZE_C));
  this->shape->setOutlineThickness(1);
  this->shape->setOutlineColor(sf::Color(0, 0, 0));
  this->settings.antialiasingLevel = 8;

  GameWindow = new sf::RenderWindow(sf::VideoMode(SIZE_W, SIZE_W), "SFML shapes", sf::Style::Default, this->settings);
  if (!this->texture.loadFromFile("skin/wood.jpg"))
    std::cerr << "Error while loading wood skin!" << std::endl;

  this->x = 0;
  this->y = 0;
  this->sprite.setTexture(this->texture);
  this->shape->setTexture(&this->texture);
  this->texture.setSmooth(true);
  GameWindow->clear(sf::Color::Black);
}

GUI::~GUI()
{

}

int		GUI::launchWindow()
{
   while (GameWindow->isOpen())
    {
      sf::Event event;
      while (GameWindow->pollEvent(event))
        {
	  if (event.type == sf::Event::Closed)
	    GameWindow->close();
	  if (event.type == sf::Event::KeyPressed)
	    {
	      if (event.key.code == sf::Keyboard::Left)
		std::cout<< "Left key pressed" << std::endl;
	      if (event.key.code == sf::Keyboard::Right)
		std::cout<< "Right key pressed" << std::endl;
	      if (event.key.code == sf::Keyboard::Up)
		std::cout<< "Up key pressed" << std::endl;
	      if (event.key.code == sf::Keyboard::Down)
		std::cout<< "Down key pressed" << std::endl;
	      if (event.key.code == sf::Keyboard::Return)
		std::cout<< "Enter key pressed" << std::endl;
	    }
        }

      while (this->x <= SIZE_W && this->y <= SIZE_W)
	{
	  this->shape->setPosition(this->x, this->y);
	  GameWindow->draw(*this->shape);
	  this->x += SIZE_C;
	  if (this->x == SIZE_C * 19)
	    {
	      this->x = 0;
	      this->y += SIZE_C;
	    }
	}
      GameWindow->display();
    }
  return 0;
}

void		GUI::setX(int _x)
{
  this->x = _x;
}

void		GUI::setY(int _y)
{
  this->y = _y;
}

void		GUI::setI(int _i)
{
  this->i = _i;
}

void		GUI::setJ(int _j)
{
  this->j = _j;
}

int		GUI::getX() const
{
  return (this->x);
}

int		GUI::getY() const
{
  return (this->y);
}

int		GUI::getI() const
{
  return (this->i);
}

int		GUI::getJ() const
{
  return (this->j);
}

bool			GUI::menuLaunch()
{
  sf::RenderWindow	window(sf::VideoMode(800, 600), "Gomoku");
  sf::Event		event;
  sf::Font		font_bebas, font_ostrich;
  sf::Music		music;
  sf::SoundBuffer	soundBuffer_button;
  sf::Sound		sound_button;
  bool			play = true;

  if (!font_bebas.loadFromFile("fonts/BEBAS.ttf"))
    return false;
  if (!font_ostrich.loadFromFile("fonts/ostrich-regular.ttf"))
    return false;

  sf::Text title("Gomoku", font_bebas, 75);
  title.setColor(sf::Color(128,128,0));
  title.move(270.f,100.f);

  sf::Text BTN_1("Jouer", font_ostrich, 50);
  BTN_1.setColor(sf::Color(0,128,0));
  BTN_1.move(340.f,280.f);

  sf::Text BTN_2("Quitter", font_ostrich, 50);
  BTN_2.setColor(sf::Color(128,128,128));
  BTN_2.move(328.f,350.f);

  if (!soundBuffer_button.loadFromFile(SOUND_BUTTON_PATH))
    return false;
  sound_button.setBuffer(soundBuffer_button);

  if (!music.openFromFile(MUSIC_MENU_PATH))
    return false;
  music.play();

  window.setFramerateLimit(15);

  while (window.isOpen())
  {
    while (window.pollEvent(event))
      {
	if (event.type == sf::Event::Closed)
	  window.close();
	else
	  {
	    if (event.type == sf::Event::KeyPressed)
	      {
		if (event.key.code == sf::Keyboard::Escape)
		  window.close();
		if (event.key.code == sf::Keyboard::Return)
		  {
		    window.close();
		    music.stop();
		    return play;
		  }
		if (event.key.code == sf::Keyboard::Down)
		  {
		    if (play)
		      {
			play = false;
			BTN_1.setColor(sf::Color(128,128,128));
			BTN_2.setColor(sf::Color(0,128,0));
			sound_button.play();
		      }
		  }
		if (event.key.code == sf::Keyboard::Up)
		  {
		    if (!play)
		      {
			play = true;
			BTN_1.setColor(sf::Color(0,128,0));
			BTN_2.setColor(sf::Color(128,128,128));
			sound_button.play();
		      }
		  }
	      }
	  }
      }
    if (music.getStatus() == 0)
      music.play();

    //    title.setColor(sf::Color(sf::Randomizer::Random(0,128), sf::Randomizer::Random(0,128),0));

    window.clear();
    window.draw(title);
    window.draw(BTN_1);
    window.draw(BTN_2);
    window.display();
  }
  return false;
}
