//
// menu.hpp for  in /home/jousse_g/rendu/gomoku_ninuki/source/menu
// 
// Made by Gautier Jousset
// Login   <jousse_g@epitech.net>
// 
// Started on  Tue Nov 24 21:21:39 2015 Gautier Jousset
// Last update Tue Nov 24 22:43:28 2015 Gautier Jousset
//

#ifndef MENU_HPP_
#define MENU_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#define	FONT_BEBAS_PATH		"fonts/BEBAS.ttf"
#define	FONT_OSTRICH_PATH	"fonts/ostrich-regular.ttf"

#define MUSIC_MENU_PATH		"music/menu.ogg"

#define SOUND_BUTTON_PATH	"sound/button.ogg"

bool	menu(void);

#endif
