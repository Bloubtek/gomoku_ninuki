//
// main.cpp for  in /home/jousse_g/Downloads/dev
// 
// Made by Gautier Jousset
// Login   <jousse_g@epitech.net>
// 
// Started on  Tue Nov 24 15:11:36 2015 Gautier Jousset
// Last update Tue Nov 24 21:27:41 2015 Gautier Jousset
//

#include <iostream>
#include "menu.hpp"

int	main()
{
  if (menu())
    {
      std::cout << "Le partie démarre" << std::endl;
      return EXIT_SUCCESS;
    }
  else
    {
      std::cout << "Vous avez quitté le jeu" << std::endl;
      return EXIT_FAILURE;
    }
}
